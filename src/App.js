import React from 'react';
import logo from './logo.svg';
import './App.css'
import './assets/css/layout.scss'
import './assets/css/themes.scss'
import './assets/css/variables.scss'
import Layout from './views/Layout/Layout'
import Home from './views/Home'
import Search from './views/Search'
import SearchBox from "./components/SearchBox/SearchBox";
import Body from "./containers/Body/Body";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Details from "./views/Details";
import LabRat from "./components/LabRat/LabRat";
function App() {
  return (
    <div className="App">
          <Layout/>
          {/*<LabRat/>*/}
    </div>
  );
}

export default App;
