import React, {Component} from 'react';
import {Link} from "react-router-dom";

const MovieCard = props =>{
  return (
      <div className="movie-card">
          <img src={props.movie.large_cover_image} alt=""/>
          <Link to={`/details/${props.movie.id}`}>
              <h3>{props.movie.title}</h3>
          </Link>
      </div>
  )
}

export default MovieCard;