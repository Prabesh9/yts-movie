import React,{useHistory} from 'react';
import {Link} from "react-router-dom";

class SearchBox extends React.Component{
    state={
        search:"",
    };
    inputHandler=e=>{
        console.log(e.target.value)
        this.setState({
            [e.target.name]:e.target.value
        })
    };
    gotoSearch=()=>{
        // console.log(this.state.search);
        // console.log(this.props);
        // this.props.history.push('/search/'+this.state.search)
    };

    render() {
        return(
            <div className="searchBox">
                <input type="text" name="search" id="" onChange={(e)=>(this.inputHandler(e))}/>
                <Link to={'/search/'+this.state.search}><button >Search</button></Link>
            </div>
        )
    }


};

export default SearchBox;