import React from 'react';

const Logo=(props)=>{
    return(
        <div className={'myLogo'}>{props.children}</div>
    )
};

export default Logo;