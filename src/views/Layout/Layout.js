import React,{Component} from 'react';
import NavBar from "../../containers/Navbar/NavBar";
import Home from "../Home";
import {Route, Switch} from "react-router-dom";
import Search from "../Search";
import Details from "../Details";
import routes from "../../AppRoutes";
import TopNavBar from "../../containers/TopNavBar/TopNavBar";
class Layout extends Component{
    state={
        navItems:[
            {id:1, label:'Home', path:''},
            {id:2, label:'Search', path:'/search'},
        ],
        isOpen:false
    };
    navHandler=()=>{
        this.setState({
            isOpen:!this.state.isOpen
        })
    };
  render(){
      return(
        <section>
            <TopNavBar {...this.props} toggleNav={this.navHandler}/>
            <main>
                <NavBar navItems={this.state.navItems} isOpen={this.state.isOpen}/>
                <div className="content-area">
                    <Switch>
                        {routes.map((route,key)=>
                           <Route path={route.path} component={route.component} exact={route.exact}/>
                        )}
                    </Switch>
                </div>
            </main>
        </section>
      )
  }
}

export default Layout;