import React, {Component} from 'react';
import abc from "../assets/image/abc.jpg"
import MovieCard from "../components/MovieCard/MovieCard";
import axios from 'axios'

class Home extends Component {
    state={
        movies:[
            {id:1, title:'Hello', small_cover_image: abc },
            {id:2, title:'Hello World', small_cover_image:abc },
            {id:3, title:'Hello Again', small_cover_image:abc },
        ],
        search:"",
    };


    componentDidMount() {
        axios.get('https://yts.lt/api/v2/list_movies.json').
        then(res=>{
                console.log(res.data.data.movies);
            this.state.movies.push(res.data.data.movies)
            this.setState({
                movies:res.data.data.movies
            })
        })
    }

    render() {
        return (
            <>

                <div className={'movie-list'}>
                    {this.state.movies.map((movie,key)=>(
                        <MovieCard movie={movie}/>
                    ))}
                </div>
            </>
        );
    }
}

export default Home;