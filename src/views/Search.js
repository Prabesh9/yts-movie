import React, {Component} from 'react';
import axios from "axios"
import abc from "../assets/image/abc.jpg";
import MovieCard from "../components/MovieCard/MovieCard";

class Search extends Component {
    state={
        movieParam:"",
        movies:[
            {id:1, title:'Hello', small_cover_image: abc },
            {id:2, title:'Hello World', small_cover_image:abc },
            {id:3, title:'Hello Again', small_cover_image:abc },
        ],
    };
    componentDidMount() {
        console.log(this.props.match.params.name);
        this.setState({
            movieParam:this.props.match.params.name
        });
        this.moviesHandler();
    }

    componentDidUpdate(prevProps) {
        if(this.props.location.pathname!==prevProps.location.pathname){
            this.moviesHandler()
        }
    }

    moviesHandler=()=>{
        axios.get("https://yts.lt/api/v2/list_movies.json?query_term="+this.props.match.params.name)
            .then(res=>{
                this.setState({
                    movies:res.data.data.movies
                })
            })
    };

    render() {
        return (
            <div className={'main-body'}>
                <div className={'movie-list'}>
                    {this.state.movies.map((movie,key)=>(
                        <MovieCard movie={movie}/>
                    ))}
                </div>
            </div>
        );
    }
}

export default Search;