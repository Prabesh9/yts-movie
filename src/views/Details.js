import React, {Component} from "react";
import axios from "axios";

class Details extends Component{
    state={
        movie:{}
    }
    componentDidMount() {
        let id = this.props.match.params.id
        axios.get('https://yts.lt/api/v2/movie_details.json?movie_id='+id).
            then((res)=>{
                console.log(res);
                this.setState({
                    movie:res.data.data.movie
                })
        });
    }
    render() {
        return(
            <div className={'main-body'}>
                <div className="left"><h1>
                    {this.state.movie.title}</h1>
                    <img src={this.state.movie.large_cover_image} alt=""/>
                </div>
                <div className="right">
                    <p>{this.state.movie.description_full}</p>
                </div>

            </div>
        )
    }
}

export default Details