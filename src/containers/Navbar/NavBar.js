import React,{Component} from 'react';
import Logo from "../../components/Logo/Logo";

class NavBar extends Component {
    render(){
        return (
            <nav className={this.props.isOpen? 'open':'close'} >
                <Logo>Lucifer</Logo>
                <NavItems navItems={this.props.navItems}/>
            </nav>
        )
    }
};

const NavItems= (props)=>{
    return (
        <div className={'nav-items'}>
            {props.navItems.map((navItem)=>(
                <NavItem key={navItem} label={navItem}/>
            ))}
        </div>
    )
};

const NavItem = (props)=>{
    return (
        <div className="nav-item">{props.label.label}</div>
    )
}
export default NavBar;