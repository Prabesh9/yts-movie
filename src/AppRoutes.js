import Home from "./views/Home";
import Search from "./views/Search";
import Details from "./views/Details";

export default [
    {path:'/search/:name', component:Search, exact:true},
    {path:'/details/:id', component:Details, exact:true},
    {path:'', component:Home, exact:true},
]
